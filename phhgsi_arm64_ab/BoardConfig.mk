include build/make/target/board/generic_arm64_ab/BoardConfig.mk
include device/phh/treble/board-base.mk

# Dex
#WITH_DEXPREOPT := false
#WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := false
WITH_DEXPREOPT := false
WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := false
DONT_DEXPREOPT_PREBUILTS := false LOCAL_DEX_PREOPT := true
PRODUCT_DEX_PREOPT_BOOT_FLAGS := --compiler-filter=speed
PRODUCT_DEX_PREOPT_DEFAULT_FLAGS := --compiler-filter=speed

