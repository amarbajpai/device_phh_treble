include build/make/target/board/generic_arm64_a/BoardConfig.mk
include device/phh/treble/board-base.mk

#BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2147483648

# Dex
WITH_DEXPREOPT := false
WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := false
